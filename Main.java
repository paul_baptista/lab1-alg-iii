/*
 * Archivo: Main.java 
 * Descripcion: Archivo de prueba. Prueba todas las
 * funcionalidades implementadas en forma ordenada y clara.
 * Autor: Paul Baptista (10-10056) y Fabio Castro (10-10132). Grupo Z
 * Fecha:
 */
import java.io.*; 

public class Main {


   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }


   /*
    * Funcion ClearScreen imprime 25 saltos de linea y limpiando el terminal
    */

   public static void ClearScreen() {
      int i;
      for(i=0;i<=25;i++){
         println();
      }

   }
   /*
    * Funcion printlist, imprime en pantalla la representación en String de los
    *   objetos que contiene la lista pasada como parametro
    */
   public static void printlist(List lista) {
      ListIterator iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      

      }
   }

   /**
   * Obtiene y compara iterativamente la cantidad de antecedentes de cada nodo
   * usando la función getNAncs implementada en DigraphHash.java
   * Los empates los resuelve haciendo comparación de Strings entre los
   * identificadores de los nodos.
   */
   public static String MasDependencias(DigraphHash grafo){

      List nodes = grafo.getNodes();
      ListIterator nodesIterator = nodes.iterator();
      Node nodo=null, nodotmp;
      int deps=0, depstmp;


      while(nodesIterator.hasNext()){
         nodotmp = (Node) nodesIterator.next();
         depstmp = grafo.getNAncs(nodotmp);

         if (nodo==null){
            nodo=nodotmp;
         }
      
         if (depstmp>deps){
            deps=depstmp;
            nodo=nodotmp;
         }
         if ((depstmp==deps)
            &&( nodotmp.toString().compareToIgnoreCase(nodo.toString())<0 )) {

            nodo=nodotmp;
         }
      }
      if (nodo==null){
         return "";
      }

      return nodo.toString();
   }



   /*
      Main Principal que ejecuta todas las pruebas necesarias
   */
   public static void main(String args[]) throws Exception{
      /*
      * Se manejan las excepciones  de la lectura de teclado
      */
         /*
         * Se lee la entrada de teclado por medio de un buffer, y se asigna a
         * una variable
         */  
      
        if (args.length!=2){
          println("Uso: Main archivoDeEntrada archivoDeSalida");
          return;
        }

        Archivo lectura_archivo = new Archivo();
        DigraphHash grafo = new DigraphHash();
        // Archivos de entrada y salida.
        String in = args[0];
        String out = args[1];
        lectura_archivo.openFile(in,out);
      do{     
        grafo.clear();
        grafo = lectura_archivo.readFile();
        lectura_archivo.writeFile(MasDependencias(grafo));
      } while(grafo.getNumVertices() > 0);

      lectura_archivo.closeFile();
   }

}