/**
 * Archivo.java
 * Clase que implementa el manejo de archivos.
 * 
 * Autores: Fabio Castro
 * Grupo: 9
 * Fecha: 26/11/2013
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Archivo {
  
  // File Reader 
  // (Nombre del archivo de entrada).
  private FileReader reader;

  // Buffered Reader 
  // (Apuntador a la próxima posición después de lo que lleva leído).
  private BufferedReader read;

  // File Writer 
  // (Nombre del archivo de salida).
  private FileWriter writer;

  // Print Writer 
  // (Apuntador a la próxima posición donde escribir).
  private PrintWriter wrote;
  
  
    
  /**
  * Constructor
  * Inicializa los atributos.
  */
  public Archivo() {
    reader = null;  
    read = null;
    writer = null;
    wrote = null;
  }
  
  public void openFile(String in, String out) throws Exception {
    /*
     * try - catch 
     * Chequea que el archivo de entrada haya sido abierto
     * correctamente.
     * De lo contrario, indica que hubo un error.
     */
    try {

  	  // Apunta el FileReader al archivo de entrada.
    	reader = new FileReader(in);
    	
    	// Apunta el BufferedReader al salida del archivo de entrada.
    	read = new BufferedReader(reader);
    	
    	// Apunta de FileWriter al archivo de salida.
    	writer = new FileWriter(out);
    	
    	// Apunta el PrintWriter al salida del archivo de salida.
    	wrote = new PrintWriter(writer);
  	
    } catch (Exception ioe) {
      System.err.println("Error abriendo el archivo de entrada.");
      System.exit(1);
    }
        
  }
        
  /**
  * readFile
  * Lee el archivo especificado en el stdin.
  */
  public DigraphHash readFile() throws Exception {
    DigraphHash grafo = new DigraphHash();
    Node nodo_lectura;
    Edge arco_lectura;
    int cantidad_nodos;
    int cant_arcos_entrantes_nodo;
    String[] linea = read.readLine().split(" ");
    int i;
    int j;

    cantidad_nodos = Integer.parseInt(linea[0]);
    for (i = 1; i <= cantidad_nodos; i ++) {
    	grafo.add(new Node(""+i));
    }
    for (i = 1; i <= cantidad_nodos; i++){
      linea = read.readLine().split(" ");
      cant_arcos_entrantes_nodo = Integer.parseInt(linea[0]);
      for(j = 1; j <= cant_arcos_entrantes_nodo; j++){
        grafo.add(new Edge(linea[j],""+i));
      }
    }
    // grafo.Imprimir();
    return grafo;  
  }
  
  public void writeFile(String elemento_a_escribir) {
    /* 
     * Escribe en la posición donde quedó el apuntador, la 
     * respuesta dada más un salto de línea.
     */ 
    wrote.println(elemento_a_escribir);
  }
  
  public void closeFile() throws Exception {

    /*
     * try - catch 
     * Chequea que los archivos de entrada y salida hayan sido
     * cerrados correctamente.
     * De lo contrario, indica que hubo un error.
     */
    try {

    	read.close();
    	wrote.close();

    } catch (Exception ioe) {
       System.err.println("Error cerrando un archivo.");
    }

  }
   
}