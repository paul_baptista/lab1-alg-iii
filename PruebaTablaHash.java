/*
 * Archivo: Main.java 
 * Descripcion: Archivo de prueba. Prueba todas las
 * funcionalidades implementadas en forma ordenada y clara.
 * Autor: X (carnet) y Y (carnet). Grupo Z
 * Fecha:
 */

public class PruebaTablaHash {
   
   
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }
   
   public static void printlist(List<String> lista) {
      ListIterator<String> iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      
      }
   }

   public static void agregarElems(List<String> lista, int n){
      for (int i = 1; i <= 10; i++) {   
      lista.add("Prueba" + i);
         if (lista.isEmpty()) {
            println("La lista esta vacia");
         } 
      }
   }

   public static void main(String args[]){

      MiTablaHash t1 = new MiTablaHash<String>(5);

      String tmp;

      for (int i=1;i<=25;i++) {
         tmp = new String("P"+i);
         println("Agregando "+tmp+" "+t1.agregar(tmp,"tmp"));
      }

      println();
      println(t1.obtenerCantidadListaOcupado()+" de "+t1.obtenerTamanoActualLista());

      println();

      for (int i=0;i<=27;i++) {
         tmp = new String("P"+i);
         println("Contiene "+tmp+" "+t1.contiene(tmp,tmp));
      }

      printlist(t1.obtenerNodos());

      MiTablaHash t2 = ((MiTablaHash)t1).clone();

      println();
      println();
      printlist(t1.ListaColisiones("pepe"));



   }
      
}
   